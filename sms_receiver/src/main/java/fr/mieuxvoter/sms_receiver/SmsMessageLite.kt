package fr.mieuxvoter.sms_receiver

import org.godotengine.godot.Dictionary

// Something to do if you can:
// Move this to its own module like sms_tools
// and make sms_receiver depend on it, somehow.

class SmsMessageLite {
    var id: String = ""
    var author: String = ""
    var message: String = ""
    var timestamp: Int = 0

    fun toDictionary(): Dictionary {
        val dictionary = Dictionary()
        dictionary["id"] = id
        dictionary["author"] = author
        dictionary["message"] = message
        dictionary["timestamp"] = timestamp
        return dictionary
    }
}