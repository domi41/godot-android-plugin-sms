@file:JvmName("SmsReceiverGodotPlugin")

package fr.mieuxvoter.sms_receiver

import android.Manifest
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import org.godotengine.godot.Dictionary
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo


/**
 * Send a signal when the device receives a SMS.
 *
 * Will provide the Singleton AndroidSmsReceiver to Gdscript.
 * Exposed methods are snake_cased to comply with Godot.
 *
 *
 * Caveats
 * -------
 *
 * SDK 23
 *
 * Encrypted SMSes (Silence/Signal) won't be understood.
 *
 * Won't be OK with Google Play.  See
 * https://developer.android.com/guide/topics/permissions/default-handlers
 */
@Suppress("unused")
class SmsReceiverGodotPlugin(godot: Godot) : GodotPlugin(godot) {

    companion object {
        const val TAG = "SmsReceiver"
        const val PERMISSIONS_REQUEST_RECEIVE_SMS = 200
        const val ACTION_RECEIVE_SMS = SmsReceiverBroadcastReceiver.ACTION
        private val RECEIVER_STARTED_SIGNAL = SignalInfo("receiver_started")
        private val RECEIVER_STOPPED_SIGNAL = SignalInfo("receiver_stopped")
        private val SMS_RECEIVED_SIGNAL = SignalInfo("sms_received", Dictionary::class.java)
    }

    // "Android" prefix is included because it makes sense from a Gdscript perspective.
    override fun getPluginName() = "AndroidSmsReceiver"

    override fun getPluginMethods() = listOf(
        "request_permission",
        "set_message_regex_filter",
        "can_receive",
        "start_receiving",
        "is_receiving",
        "stop_receiving"
    )

    override fun getPluginSignals() = mutableSetOf(
        RECEIVER_STARTED_SIGNAL,
        RECEIVER_STOPPED_SIGNAL,
        SMS_RECEIVED_SIGNAL
    )


    private var isReceiving: Boolean = false
    private var receiver: BroadcastReceiver? = null
    private var message_regex_filter: String = "^.*$"


    private fun requestReceivingPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val grant: Int =
                activity!!.baseContext.checkSelfPermission(Manifest.permission.RECEIVE_SMS)
            if (grant != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Prompting for RECEIVE_SMS permission…")
                activity!!.requestPermissions(
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    PERMISSIONS_REQUEST_RECEIVE_SMS
                )
                return false
            }
            return true
        } else {
            return false // how to support older versions here?
        }
    }


    /**
     * Pop the permission request window, if we're not allowed already.
     */
    @Suppress("FunctionName")
    fun request_permission() {
        requestReceivingPermissions()
    }


    /**
     * Only the SMS whose message matches this regex will be handled.
     *
     * - Escaping: not sure how it works
     * - Regex will match each line individually (MULTILINE behavior)
     */
    @Suppress("FunctionName")
    fun set_message_regex_filter(regex_to_match: String) {
        message_regex_filter = regex_to_match
    }


    /**
     * Will request the permission if not allowed.
     * Returns the permission state BEFORE the permission request popin.
     */
    @Suppress("FunctionName")
    fun can_receive(): Boolean {
        return requestReceivingPermissions()
    }


    @Suppress("FunctionName")
    fun start_receiving() {
        if ( ! can_receive()) {
            Log.w(TAG, "Cannot start, not allowed.")
            return
        }
        if (is_receiving()) {
            Log.w(TAG, "Cannot start, already receiving.")
            return
        }

        isReceiving = true

        receiver =
            SmsReceiverBroadcastReceiver(
                object :
                    SmsReceiverHandler() {
                    override fun shouldHandleSms(sms: SmsMessageLite): Boolean {
                        if ( ! isReceiving) {
                            return false
                        }
                        val regex = Regex(message_regex_filter, setOf(RegexOption.MULTILINE))
                        return regex.containsMatchIn(sms.message)
                    }

//                    override fun shouldVoidSms(sms: SmsMessageLite): Boolean {
//                        return true // unused anyway, unless we resort to root tricks
//                    }

                    override fun handleSms(sms: SmsMessageLite) {
                        emitSignal(SMS_RECEIVED_SIGNAL.name, sms.toDictionary())
                    }
                })
        val intentFilter = IntentFilter(ACTION_RECEIVE_SMS)
        intentFilter.priority = IntentFilter.SYSTEM_HIGH_PRIORITY - 1
        activity!!.applicationContext.registerReceiver(
            receiver,
            intentFilter
            // (perhaps) use broadcastPermission filter, for added security
        )

        emitSignal(RECEIVER_STARTED_SIGNAL.name)
    }


    @Suppress("FunctionName")
    fun is_receiving(): Boolean {
        return isReceiving
    }


    @Suppress("FunctionName")
    fun stop_receiving() {
        if ( ! is_receiving()) {
            Log.w(TAG, "Cannot close receiver, it was not started.")
            return
        }

        isReceiving = false

        activity!!.applicationContext.unregisterReceiver(receiver)
        receiver = null

        emitSignal(RECEIVER_STOPPED_SIGNAL.name)
    }


}