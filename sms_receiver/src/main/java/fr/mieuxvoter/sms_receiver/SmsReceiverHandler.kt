package fr.mieuxvoter.sms_receiver

/**
 *
 */
abstract class SmsReceiverHandler {
    /**
     * Typically, check the SMS message against a Regex, or the author against a database…
     */
    abstract fun shouldHandleSms(sms: SmsMessageLite): Boolean

    /**
     * SMS will be handled first, and then voided.
     * Only handled SMSes may be voided.
     * We can't void SMS for other applications (since 4.4), so this is not very useful.
     */
//    abstract fun shouldVoidSms(sms: SmsMessageLite): Boolean

    /**
     * Applied to SMSes that we should handle. (see shouldHandleSMS())
     * The SMS should not be mutated ; mutations won't propagate to other SMS receivers.
     * They perhaps could, if we truly wanted, but I don't need it, so you do it !  :]
     */
    abstract fun handleSms(sms: SmsMessageLite)
}