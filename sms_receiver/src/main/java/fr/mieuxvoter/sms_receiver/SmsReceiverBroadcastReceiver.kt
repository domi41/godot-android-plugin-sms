package fr.mieuxvoter.sms_receiver


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log


class SmsReceiverBroadcastReceiver(var handler: fr.mieuxvoter.sms_receiver.SmsReceiverHandler) : BroadcastReceiver() {

    companion object {
        const val ACTION = "android.provider.Telephony.SMS_RECEIVED"
        const val TAG = "SmsReceiver"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, "Receiving a new SMS…")
        if (null == intent) {
            Log.w(TAG, "Received a null Intent.  Skipping…")
            return
        }
        if (null == intent.action) {
            Log.w(TAG, "Received a null Intent action.  Skipping…")
            return
        }
        if (ACTION.compareTo(intent.action!!, true) != 0) {
            Log.d(TAG, "Received a Intent we don't care about.  Skipping…")
            return
        }

        Log.d(TAG, "Reading the new SMS…")
        // PDUS: Protocol Description UnitS
        val pdus = intent.extras!!["pdus"] as Array<*>?
        if (null == pdus) {
            Log.w(TAG, "No PDUS in the SMS at all!")
            return
        }
        if (pdus.isEmpty()) {
            Log.w(TAG, "No PDUS in the SMS!")
            return
        }
        val messages: Array<SmsMessage?> = arrayOfNulls(pdus.size)
        for (i in pdus.indices) {
            messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
        }

        val sms = SmsMessageLite()
        sms.author = messages[0]!!.displayOriginatingAddress
        sms.timestamp = (messages[0]!!.timestampMillis / 1000.0).toInt()
        // SMS may have several parts, let's combine them
        val smsBody = StringBuilder()
        for (i in messages.indices) {
            if (null != messages[i]) {
                smsBody.append(messages[i]!!.messageBody)
            } else {
                Log.w(TAG, String.format("PDU #%d/%d failed to be parsed!", i+1, pdus.size))
            }
        }
        sms.message = smsBody.toString()

        // Only uncomment this when debugging, it's a security/privacy issue
        //Log.d(TAG, String.format("Read SMS from %s: %s", sms.author, sms.message))

        if (handler.shouldHandleSms(sms)) {
            Log.d(TAG, "Handling that SMS…")
            handler.handleSms(sms)
            // NOPE: https://stackoverflow.com/questions/50351444/abortbroadcast-not-working-in-smsreceiver-broadcast#
//            if (handler.shouldVoidSms(sms)) {
//                Log.d(TAG, "Spoiling that SMS…")
//                abortBroadcast()
//            }
        } else {
            Log.d(TAG, "SMS was not handled.")
        }

    }

    // Snippet for later, perhaps
//    fun showAlert(context: Context, title: String = "", message: String = "") {
//        val dialogBuilder = AlertDialog.Builder(context)
//        dialogBuilder.setMessage(message)
//            .setCancelable(false)
//            .setPositiveButton("Indeed", DialogInterface.OnClickListener {
//                    dialog, _ -> dialog.cancel()
//            })
//        val alert = dialogBuilder.create()
//        alert.setTitle(title)
//        alert.show()
//    }
}