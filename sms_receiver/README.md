SMS Receiver
============

Godot Android Plugin that emits a signal when a SMS was received.

Only works when the app is still alive.
Remember, android may kill at any time an app that lost focus.

Example Usage
-------------

```gdscript
var AndroidSmsReceiver

func _ready():
	if Engine.has_singleton('AndroidSmsReceiver'):
		AndroidSmsReceiver = Engine.get_singleton('AndroidSmsReceiver')
		AndroidSmsReceiver.start_receiving()
		AndroidSmsReceiver.connect('sms_received', this, '__on_sms_received')

func _exit_tree():
	if AndroidSmsReceiver:
		AndroidSmsReceiver.stop_receiving()

func __on_sms_received(sms:Dictionary):
	print(str(sms))
```

> Possibly a very bad code example. (memleaks, etc.)  Help!