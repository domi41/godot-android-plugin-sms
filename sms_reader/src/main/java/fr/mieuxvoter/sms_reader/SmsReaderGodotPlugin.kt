@file:JvmName("SmsReaderGodotPlugin")

package fr.mieuxvoter.sms_reader

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.provider.Telephony.Sms
import org.godotengine.godot.Dictionary
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin


/**
 * Reads SMS from the Inbox, most recent first.
 *
 * Will provide the Singleton AndroidSmsReader to Gdscript.
 * Exposed methods are snake_cased to comply with Godot.
 *
 *
 * Caveats
 * -------
 *
 * BUGS; Hairy. Gooey. Hungry. Eating away at deprecated cables.
 *
 * We don't use this anymore.
 *
 * SDK 23
 *
 * Silence/Signal won't work ; this reads Messenger.
 *
 * Won't be OK with Google Play.  See
 * https://developer.android.com/guide/topics/permissions/default-handlers
 */
@Suppress("unused")
class SmsReaderGodotPlugin(godot: Godot) : GodotPlugin(godot) {

    companion object {
        const val PERMISSIONS_REQUEST_READ_SMS = 100
    }

    // "Android" prefix is included because it makes sense from a Gdscript perspective.
    override fun getPluginName() = "AndroidSmsReader"

    override fun getPluginMethods() = listOf<String>(
        "can_read",
        "read"
    )

    private fun requestReadingPermissions(): Boolean {
        val c : Context = activity!!.baseContext

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val sms: Int = c.checkSelfPermission(Manifest.permission.READ_SMS)

            if (sms != PackageManager.PERMISSION_GRANTED) {
                activity!!.requestPermissions(
                    arrayOf<String>(Manifest.permission.READ_SMS),
                    PERMISSIONS_REQUEST_READ_SMS
                )
                return false
            }
            return true
        } else {
            return false
        }
    }

    @Suppress("FunctionName")
    fun can_read(): Boolean {
        return requestReadingPermissions()
    }

    @Suppress("FunctionName")
    fun read(since: Int = 0, skip: Int = 0, limit: Int = 32): Dictionary {
        if ( ! can_read()) {
            return Dictionary()
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return Dictionary()
        }

        val smses = Dictionary()

        val smsInboxUri = Sms.CONTENT_URI
        val smsProps = arrayOf(Sms._ID, Sms.DATE, Sms.TYPE, Sms.ADDRESS, Sms.BODY)
        val smsCondition = String.format(
            "%s = %d AND %s >= %d",
            Sms.TYPE, Sms.MESSAGE_TYPE_INBOX,
            Sms.DATE, since
        )

        val c: Cursor = activity!!.contentResolver.query(
            smsInboxUri,
            smsProps,
            smsCondition,
            null,
            null
        ) ?: return Dictionary()
        // FIXME: SOMETHING HORRIBLE IS LURKING HERE… (Activity crashes when resuming)
        activity!!.startManagingCursor(c) // use CursorLoader instead

        val smsTotalCount: Int = c.count
        val smsToSkip: Int = skip.coerceAtMost(smsTotalCount)
        val smsToReadCount: Int = smsTotalCount.coerceAtMost(limit)
        if (c.moveToFirst()) {
            var thereIsMore: Boolean = true
            for (i in 0 until smsToSkip) {
                thereIsMore = c.moveToNext()
                if ( ! thereIsMore) {
                    break
                }
            }
            if (thereIsMore) {
                for (i in 0 until smsToReadCount) {
                    val sms = Dictionary()
                    sms["id"] = c.getInt(c.getColumnIndexOrThrow(Sms._ID))
                    sms["timestamp"] = c.getInt(c.getColumnIndexOrThrow(Sms.DATE))
                    sms["author"] = c.getString(c.getColumnIndexOrThrow(Sms.ADDRESS))
                    sms["message"] = c.getString(c.getColumnIndexOrThrow(Sms.BODY))
                    smses[String.format("%03d (%03d)", sms["id"], i)] = sms
                    thereIsMore = c.moveToNext()
                    if ( ! thereIsMore) {
                        break
                    }
                }
            }
        }
        c.close()

        return smses
    }

}