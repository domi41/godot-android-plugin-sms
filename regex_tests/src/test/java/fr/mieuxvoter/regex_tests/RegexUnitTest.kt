package fr.mieuxvoter.regex_tests

import org.junit.Test

import org.junit.Assert.*

/**
 * Checking my assumptions about Kotlin Regexes
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class RegexUnitTest {
    @Test
    fun matches() {
        val regex = Regex("""^([a-zA-Z]{1,2}[0-9]{1,}[^a-zA-Z]*)+""")
        val regem = Regex("""^([a-zA-Z]{1,2}[0-9]{1,}[^a-zA-Z]*)+""", setOf(RegexOption.MULTILINE))
        assertFalse(regex.containsMatchIn(""))
        assertFalse(regex.containsMatchIn("Nope"))
        assertFalse(regex.containsMatchIn("Z"))
        assertTrue(regex.containsMatchIn("A2  B1c4"))
        assertFalse(regex.containsMatchIn("Just vote like: A2  B1c4"))

        assertFalse(regex.containsMatchIn("Awesome!\nA2"))
        assertTrue(regem.containsMatchIn("Awesome!\nA2"))
        assertFalse(regem.containsMatchIn("Awesome!\nMagic!"))
    }
}