
Repository for the `AndroidSmsReceiver` Godot Android Plugin.


Used in the Godot-made Android application _"Majority Judgment (for Streamers)"_,
here : https://git.mieuxvoter.fr/MieuxVoter/majority-judgment-for-streamers

Also holds a buggy `AndroidSmsReader` that we don't use.

We generate the `*.aar` files using Android Studio and this project.
We included the godot `aar` library for simplicity.  _Advice welcome._
